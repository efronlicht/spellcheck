#[cfg(test)]
mod test_dict;
use std::borrow::Borrow;
use std::collections::HashMap;
use std::fs::File;
use std::hash::Hash;
use std::io;
use std::iter::FromIterator;
use std::path::Path;
extern crate stringedits;
use self::stringedits::Edit;

/// the ascii characters from 'a'..='z'
pub const ASCII_LOWERCASE: &str = "abcdefghijklmnopqrstuvwxyz";
/// A Result type with IOError
pub type Result<T> = std::result::Result<T, IOError>;
#[derive(Debug)]
// An error saving or loading a dictionary
pub enum IOError {
    /// An actual io error
    IO(io::Error),
    /// An error parsing the count
    ParseInt(usize, std::num::ParseIntError),
    /// A line of the input file was not in the form "$WORD $COUNT"
    BadFormat(usize),
}
#[derive(Clone, Debug, PartialEq, Eq)]
/// Dict is a spellchecking dictionar, that returns suggested words with levenstein distance 1 or 2 using it's [Dict::correction] method.
/// It defaults to using the ASCII_LOWERCASE alphabet for it's edits; see [stringedits::Edit] for more details on alphabets & edits.
/// All values in the dictionary are _nonnegative integers_; index access **does not panic** on a missing key,
/// but instead returns zero.
/// The default alphabet is [ASCII_LOWERCASE]; that is, 'a..=z'
/// ```
/// # use spellcheck_toy::Dict;
/// let mut dict = Dict::default();
/// dict.insert("foo");
/// dict.insert("foo");
/// dict.insert("bar");
/// assert_eq!(dict["foo"], 2);
/// assert_eq!(dict["baz"], 0);
/// ```
pub struct Dict {
    map: HashMap<String, usize>,
    alphabet: Box<[char]>,
}

fn charslice(s: impl AsRef<str>) -> Box<[char]> {
    s.as_ref().chars().collect::<Vec<char>>().into_boxed_slice()
}

impl Default for Dict {
    /// Create a new, empty dictionary, using [ASCII_LOWERCASE] as it's alphabet
    fn default() -> Self {
        Dict::new_ascii_lowercase()
    }
}

impl Dict {
    /// Retrieve the number of times a word appears in the dictionary,
    /// ```
    /// # use spellcheck_toy::*;
    /// let custom_dict: Dict = Dict::from_corpus("the quick brown fox jumped over the lazy dog".split_whitespace(), ASCII_LOWERCASE);
    /// assert_eq!(custom_dict.get("the"), Some(&2));
    /// assert_eq!(custom_dict.get("foobar"), None);
    /// ```
    pub fn get<Q>(&self, word: &Q) -> Option<&usize>
    where
        String: Borrow<Q>,
        Q: Eq + Hash + ?Sized,
    {
        self.map.get(word)
    }

    pub fn new(alphabet: impl AsRef<str>) -> Self {
        Dict {
            map: HashMap::default(),
            alphabet: charslice(alphabet),
        }
    }
    /// Create a new, empty dictionary, using [ASCII_LOWERCASE] as it's alphabet
    pub fn new_ascii_lowercase() -> Self {
        Dict::new(ASCII_LOWERCASE)
    }

    ///the most likely correction for a word, if any. If the word exists in the dictionary, chooses that word.
    /// Otherwise, prioritizes the most common distance-1 correction,
    /// then the most common distance-2 correction, if no distance-1 correction exists.
    /// ```
    /// # use spellcheck_toy::*;
    /// let custom_dict: Dict = Dict::from_corpus("the quick brown fox jumped over the lazy dog".split_whitespace(), ASCII_LOWERCASE);
    /// assert_eq!(custom_dict.correction("brown"), Some("brown".to_string())); // distance 0
    /// assert_eq!(custom_dict.correction("fxo"), Some("fox".to_string())); // distance 1
    /// assert_eq!(custom_dict.correction("thnn"), Some("the".to_string())); // distance 2
    /// assert_eq!(custom_dict.correction("amklsdmalskdnasklfn"), None); // distance... a lot
    /// ```
    pub fn correction(&self, word: impl AsRef<str>) -> Option<String> {
        let k = word.as_ref();
        if self.map.contains_key(k) {
            Some(k.to_string())
        } else if let Some(dist_1_correction) = word
            .dist1edits(&self.alphabet)
            // if there is a distance-1 correction, we don't bother checking distance-2 corrections
            .filter(|x| self.map.contains_key(x))
            .max_by_key(|x| self.map[x])
        {
            Some(dist_1_correction)
        } else {
            word.dist2edits(&self.alphabet)
                .filter(|x| self.map.contains_key(x))
                .max_by_key(|x| self.map[x])
        }
    }
    /// insert a word into the dictionary, incrementing it's count by one.
    /// ```
    /// # use spellcheck_toy::*;
    /// let mut dict = Dict::default();
    /// dict.insert("foo");
    /// dict.insert("bar");
    /// dict.insert("baz".to_string());
    /// dict.insert("bar");
    /// assert_eq!(dict["bar"], 2);
    /// assert_eq!(dict["baz"], 1);
    /// ```
    pub fn insert(&mut self, word: impl AsRef<str>) {
        if let Some(v) = self.map.get_mut(word.as_ref()) {
            *v += 1;
            return;
        }
        self.map.insert(word.as_ref().to_string(), 1);
    }
    /// create a new dictionary from a corpus of text.
    /// alias for [FromIterator::from_iter]
    /// ```
    /// # use spellcheck_toy::*;
    /// let custom_dict = Dict::from_corpus("the quick brown fox jumped over the lazy dog".split_whitespace(), ASCII_LOWERCASE);
    /// ```
    pub fn from_corpus<I>(corpus: I, alphabet: impl AsRef<str>) -> Self
    where
        I: IntoIterator,
        I::Item: AsRef<str>,
    {
        let mut count: HashMap<String, usize> = HashMap::new();

        for k in corpus.into_iter() {
            if let Some(v) = count.get_mut(k.as_ref()) {
                *v += 1;
                continue;
            }

            count.insert(k.as_ref().to_string(), 1);
        }
        Dict {
            map: count,
            alphabet: charslice(alphabet),
        }
    }

    /// Save a dictionary to the specified file. See [Dict::save_to_writer]
    pub fn save_to_file<P: AsRef<Path>>(&self, path: P) -> io::Result<()> {
        self.save_to_writer(io::BufWriter::new(File::create(path)?))
    }

    /// save a dictionary to a writer in the space-separated format
    /// 'word count'
    /// i.e
    /// ```
    /// const TEST_STR: &str = (
    /// "the 23135851162
    /// of 13151942776
    /// and 12997637966
    /// ");
    /// # use spellcheck_toy::*;
    /// # use std::io;
    /// let f = io::BufReader::new(TEST_STR.as_bytes());
    /// let dict = Dict::load_from_reader(f, ASCII_LOWERCASE).unwrap();
    /// let mut buf = io::BufWriter::new(Vec::with_capacity(200));
    /// dict.save_to_writer(&mut buf).unwrap();
    /// let got_str = String::from_utf8(buf.into_inner().unwrap()).unwrap();
    /// assert_eq!(TEST_STR, got_str)
    /// ```
    pub fn save_to_writer(&self, mut w: impl io::Write) -> io::Result<()> {
        let mut buf: Vec<_> = self.map.iter().collect();
        buf.sort_by(|(k0, v0), (k1, v1)| v1.cmp(v0).then_with(|| k0.cmp(k1)));
        for (k, v) in buf {
            writeln!(w, "{} {}", k, v)?
        }
        Ok(())
    }
    /// Load a dictionary from the specified file. See [Dict::load_from_reader]
    pub fn load_from_file<P: AsRef<Path>>(path: P, alphabet: impl AsRef<str>) -> Result<Self> {
        Dict::load_from_reader(&mut io::BufReader::new(File::open(path)?), alphabet)
    }

    /// Load a dictionary from a reader. See [Dict::save_to_writer].
    pub fn load_from_reader(r: impl io::BufRead, alphabet: impl AsRef<str>) -> Result<Self> {
        Ok(Dict {
            map: r
                .lines()
                .enumerate()
                .map(|(i, line_res)| {
                    let line = line_res?;
                    let mut split = line.split_whitespace();
                    match (split.next(), split.next()) {
                        (Some(word), Some(count)) => match count.parse::<usize>() {
                            Err(err) => Err(IOError::ParseInt(i, err)),
                            Ok(count) => Ok((word.to_string(), count)),
                        },
                        _ => Err(IOError::BadFormat(i)),
                    }
                })
                .collect::<Result<HashMap<String, usize>>>()?,
            alphabet: charslice(alphabet),
        })
    }
}

impl From<io::Error> for IOError {
    fn from(err: io::Error) -> Self {
        IOError::IO(err)
    }
}

impl<S: AsRef<str>> std::iter::Extend<S> for Dict {
    fn extend<I: IntoIterator<Item = S>>(&mut self, it: I) {
        for s in it {
            self.insert(s)
        }
    }
}
impl std::fmt::Display for IOError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            IOError::IO(err) => err.fmt(f),
            IOError::ParseInt(line, err) => {
                write!(f, "error parsing count of word on line {}: {}", line, err)
            }
            IOError::BadFormat(line) => write!(f, "could not parse line {}", line),
        }
    }
}

impl std::error::Error for IOError {}

impl<'a, Q: ?Sized> std::ops::Index<&'a Q> for Dict
where
    String: Borrow<Q>,
    Q: Eq + Hash,
{
    type Output = usize;
    fn index(&self, q: &Q) -> &usize {
        if let Some(v) = self.get(q) {
            v
        } else {
            &0
        }
    }
}
